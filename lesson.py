from fastapi import FastAPI
import uvicorn

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello, World!"}

@app.get("/new")
async def new():
    return {"message": "New message"}

uvicorn.run(app)